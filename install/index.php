<?php

use \Bitrix\Main\EventManager;

IncludeModuleLangFile(__FILE__);
class initstudio_core extends CModule
{
    const MODULE_ID = 'initstudio.core';
    var $MODULE_ID = 'initstudio.core';
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $MODULE_CSS;
    var $strError = '';

    function __construct()
    {
        $arModuleVersion = array();
        include(dirname(__FILE__) . "/version.php");
        $this->MODULE_VERSION = $arModuleVersion["VERSION"];
        $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        $this->MODULE_NAME = GetMessage("initstudio.core_MODULE_NAME");
        $this->MODULE_DESCRIPTION = GetMessage("initstudio.core_MODULE_DESC");

        $this->PARTNER_NAME = GetMessage("initstudio.core_PARTNER_NAME");
        $this->PARTNER_URI = GetMessage("initstudio.core_PARTNER_URI");
    }

    function InstallEvents()
    {
        $eventManager = EventManager::getInstance();
        // $eventManager->registerEventHandler(
        //     'main',
        //     "OnPageStart",
        //     $this->MODULE_ID,
        //     'Initstudio\Core\Autoloader',
        //     "init"
        // );
        return true;
    }

    function UnInstallEvents()
    {
        $eventManager = EventManager::getInstance();
        // $eventManager->unRegisterEventHandler(
        //     'main',
        //     "OnPageStart",
        //     $this->MODULE_ID,
        //     'Initstudio\Core\Autoloader',
        //     "init"
        // );
        return true;
    }

    function DoInstall()
    {
        global $APPLICATION;
        $this->InstallEvents();
        RegisterModule(self::MODULE_ID);
    }

    function DoUninstall()
    {
        global $APPLICATION;
        UnRegisterModule(self::MODULE_ID);
        $this->UnInstallEvents();
    }
}
