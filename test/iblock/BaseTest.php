<?php

declare(strict_types=1);

// require(__DIR__ . "/../../../main/include/prolog_before.php");

require __DIR__ . "/../../lib/autoloader.php";

require __DIR__ . "/../../vendor/autoload.php";

use Initstudio\Core\Autoloader;
use Initstudio\Core\IBlock\Base;
use PHPUnit\Framework\TestCase;

class BaseTest extends TestCase
{
    public function testId()
    {
        Autoloader::addNamespace('Initstudio\Core')
            ->setRootDir(__DIR__ . "/../../lib")
            ->register();
        $this->assertClassHasAttribute('id', Base::class);
    }

    /**
     * @dataProvider setIdProvider
     */
    // public function testSetId($id)
    // {
    // }

    // public function setIdProvider()
    // {
    //     return [
    //         'empty' => null,
    //         'intType' => 0,
    //         'stringType' => '0',
    //         'string' => 'qwe'
    //     ];
    // }
}
