<?php

use Initstudio\Core\Events\EventList;
use Initstudio\Core\Events\EventManager;

EventManager::initAutoloadEvents(EventList::EVENTS);
