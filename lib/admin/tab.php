<?php

declare(strict_types=1);

namespace Initstudio\Core\Admin;

use ArrayAccess;
use Initstudio\Core\Admin\Options\Option;
use Iterator;

class Tab implements ArrayAccess
{
    /**
     * Идентификатор вкладки
     *
     * @var string|null
     */
    private ?string $div = null;

    /**
     * Надпись на вкладке
     *
     * @var string|null
     */
    private ?string $tabTitle = null;

    private ?string $icon = null;

    /**
     * Заголовок вкладки
     *
     * @var string|null
     */
    private ?string $title = null;

    /**
     * Список полей
     *
     * @var \Initstudio\Core\Admin\Options\Option[]
     */
    private array $optionList = [];

    private bool $closed = false;

    private array $alias = [];

    public function __construct()
    {
        $this->alias = [
            'DIV' => &$this->div,
            'TAB' => &$this->tabTitle,
            'ICON' => &$this->icon,
            'TITLE' => &$this->title,
            'OPTIONS' => &$this->optionList,
            '_closed' => &$this->closed,
        ];
    }

    public function addOption(Option $option): self
    {
        $this->optionList[] = $option;

        return $this;
    }

    /**
     * @return \Initstudio\Core\Admin\Options\Option[]
     */
    public function getOptions(): array
    {
        return $this->optionList;
    }

    /**
     * Set идентификатор вкладки
     *
     * @param  string|null  $div  Идентификатор вкладки
     *
     * @return  self
     */
    public function setDiv($div)
    {
        $this->div = $div;

        return $this;
    }

    /**
     * Set надпись на вкладке
     *
     * @param  string|null  $tabTitle  Надпись на вкладке
     *
     * @return  self
     */
    public function setTabTitle($tabTitle)
    {
        $this->tabTitle = $tabTitle;

        return $this;
    }

    /**
     * Set the value of icon
     *
     * @return  self
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Set заголовок вкладки
     *
     * @param  string|null  $title  Заголовок вкладки
     *
     * @return  self
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    public function offsetExists($offset)
    {
    }

    public function offsetGet($offset)
    {
        return $this->alias[$offset] ?? null;
    }

    public function offsetSet($offset, $value)
    {
        if (\in_array($offset, $this->alias)) {
            $this->alias[$offset] = $value;
        }
    }

    public function offsetUnset($offset)
    {
    }
}
