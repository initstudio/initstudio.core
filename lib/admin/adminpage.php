<?php

declare(strict_types=1);

namespace Initstudio\Core\Admin;

use Bitrix\Main\Context;
use Initstudio\Core\Admin\Tabs;
use Initstudio\Core\Template\Html;

class AdminPage
{
    private Tabs $tabs;
    private string $moduleId;

    public function __construct(Tabs $tabs, string $moduleId)
    {
        $this->tabs = $tabs;
        $this->moduleId = $moduleId;
    }

    public function showForm(): void
    {
        $this->tabs->start();

        echo $this->formStart();
        echo bitrix_sessid_post();
        foreach ($this->tabs->getTabs() as $tab) {
            $this->tabs->next();

            foreach ($tab->getOptions() as $option) {
                $option->showValue($this->moduleId);
            }
        }
        $this->tabs->showButton();
        $this->tabs->end();
        echo $this->formEnd();
    }

    private function formStart()
    {

        $attrs = Html::attrList([
            'method' => 'POST',
            'action' => $this->getUri(),
            'name' => $this->moduleId,
        ]);

        return '<form ' . $attrs . '>';
    }

    private function formEnd()
    {
        return '</form>';
    }

    public function saveOptions()
    {
        if (check_bitrix_sessid()) {
            foreach ($this->tabs->getTabs() as $tab) {
                foreach ($tab->getOptions() as $option) {
                    $option->save($this->moduleId);
                }
            }
        }
    }

    private function getUri(): string
    {
        $context = Context::getCurrent();
        return $context->getRequest()->getServer()->getScriptName() . '?'
            . http_build_query([
                'mid' => $this->moduleId,
                'lang' => $context->getLanguage(),
                'mid_menu' => 1,
                'tabControl_active_tab' => $context->getRequest()->getRaw('tabControl_active_tab')
            ]);
    }

    public function redirect(): void
    {
        LocalRedirect($this->getUri());
    }
}
