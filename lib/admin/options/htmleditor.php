<?php

declare(strict_types=1);

namespace Initstudio\Core\Admin\Options;

use Bitrix\Main\Config\Option as ConfigOption;
use Bitrix\Main\Loader;
use CFileMan;
use Initstudio\Core\Admin\Options\Option;


class HTMLEditor extends Option
{
    public function __construct()
    {
        Loader::includeModule('fileman');
    }

    protected function getParams(): string
    {
        return $this->default;
    }

    public function showValue(string $moduleId): void
    {
?>
        <tr>
            <td>
                <?= $this->title; ?>
            </td>
            <td>
                <?php CFileMan::AddHTMLEditorFrame(
                    $this->id,
                    $this->getValue($moduleId, $this->id),
                    $this->id . "_TYPE",
                    'html',
                    ['height' => 100, 'width' => '100%']
                ); ?>
            </td>
        </tr>
<?php
    }

    private function getValue(string $moduleId, string $name): string
    {
        return ConfigOption::get($moduleId, $name);
    }

    public function save(string $moduleId): void
    {
        ConfigOption::set($moduleId, $this->id, $this->getParams());
    }
}
