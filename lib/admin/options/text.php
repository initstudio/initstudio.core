<?php

declare(strict_types=1);

namespace Initstudio\Core\Admin\Options;

use Initstudio\Core\Admin\Options\Option;

class Text extends Option
{
    private static string $type = 'text';
    private int $size = 0;

    protected function getParams(): array
    {
        return [
            $this->id,
            $this->title,
            $this->default,
            [
                static::$type,
                $this->size
            ]
        ];
    }

    /**
     * Get the value of size
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set the value of size
     *
     * @return  self
     */
    public function setSize(int $size)
    {
        $this->size = $size;

        return $this;
    }
}
