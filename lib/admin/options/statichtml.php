<?php

declare(strict_types=1);

namespace Initstudio\Core\Admin\Options;

use Initstudio\Core\Admin\Options\Option;

class StaticHTML extends Option
{
    protected function getParams(): array
    {
        return [
            $this->id,
            $this->title,
            $this->default,
            ['statichtml']
        ];
    }
}
