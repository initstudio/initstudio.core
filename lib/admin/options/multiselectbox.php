<?php

declare(strict_types=1);

namespace Initstudio\Core\Admin\Options;

class Multiselectbox extends Selectbox
{
    protected static string $type = 'multiselectbox';
}
