<?php

declare(strict_types=1);

namespace Initstudio\Core\Admin\Options;

use Initstudio\Core\Admin\Options\Option;

class Title extends Option
{
    protected function getParams(): string
    {
        return $this->default;
    }
}
