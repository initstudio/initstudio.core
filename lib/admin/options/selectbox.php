<?php

declare(strict_types=1);

namespace Initstudio\Core\Admin\Options;

class Selectbox extends Option
{
    protected static string $type = 'selectbox';

    /**
     * Список полей для выпадющего списка
     *
     * @var array<string,string>
     */
    protected array $options = [];

    protected function getParams()
    {
        return [
            $this->id,
            $this->title,
            $this->default,
            [
                static::$type,
                $this->options
            ]
        ];
    }

    /**
     * Добавляет пункт в выпадающий список
     *
     * @param array<string,string>|array<int,array<string,string>> $option
     *
     * @return static
     */
    public function addOption(array ...$options): self
    {
        $this->options = \array_merge($this->options, ...$options);

        return $this;
    }
}
