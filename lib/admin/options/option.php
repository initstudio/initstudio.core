<?php

declare(strict_types=1);

namespace Initstudio\Core\Admin\Options;

abstract class Option
{
    protected ?string $id = null;
    protected ?string $title = null;
    protected $default;

    abstract protected function getParams();

    /**
     * @param string $id
     *
     * @return static
     */
    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @param mixed $default
     *
     * @return static
     */
    public function setDefault($default): self
    {
        $this->default = $default;

        return $this;
    }

    public function showValue(string $moduleId): void
    {
        \__AdmSettingsDrawRow($moduleId, $this->getParams());
    }

    /**
     * @param string $title
     *
     * @return static
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function save(string $moduleId): void
    {
        \__AdmSettingsSaveOption($moduleId, $this->getParams());
    }
}
