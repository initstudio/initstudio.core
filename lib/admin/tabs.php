<?php

declare(strict_types=1);

namespace Initstudio\Core\Admin;

use CAdminTabControl;
use \Initstudio\Core\Admin\Tab;

class Tabs
{
    /**
     * Имя набора вкладок
     *
     * @var string
     */
    private string $name;

    /**
     * Экземпляр класса для вывод формы с владками
     *
     * @var \CAdminTabControl|null
     */
    private ?CAdminTabControl $tabControl = null;

    /**
     * Список кнопок
     *
     * @var array<string,bool>
     */
    private array $buttons = [];

    /**
     * Список табов
     *
     * @var \Initstudio\Core\Admin\Tab[]
     */
    private array $tabList = [];

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * Добавляет вкладку в массив
     *
     * @param \Initstudio\Core\Admin\Tab $tab
     *
     * @return static
     */
    public function addTab(Tab $tab): self
    {
        $this->tabList[] = $tab;

        return $this;
    }

    /**
     * Возвращает массив экземпляров табов
     * 
     * @return \Initstudio\Core\Admin\Tab[]
     */
    public function getTabs(): array
    {
        return $this->tabList;
    }

    /**
     * Создаёт экземпляр класса CAdminTabControl
     *
     * @return static
     */
    private function create(): self
    {
        $this->tabControl = new \CAdminTabControl($this->name, $this->tabList);

        return $this;
    }

    /**
     * Обозначает начало вывода формы
     *
     * @return static
     */
    public function start(): self
    {
        if (!$this->tabControl) {
            $this->create();
        }

        $this->tabControl->Begin();

        return $this;
    }

    /**
     * Обозначает начало новогой вкладки
     *
     * @return self
     */
    public function next(): self
    {
        $this->tabControl->BeginNextTab();

        return $this;
    }

    /**
     * Добавляет кнопки к форме
     *
     * @param Array<Button::SAVE|Button::APPLY|Button::CANCEL|Button::SAVE_AND_ADD> ...$arButton
     *
     * @return
     */
    public function setButton(...$arButton)
    {
        $buttonsAliac = [
            'btnSave',
            'btnApply',
            'btnCancel',
            'btnSaveAndAdd',
        ];

        foreach ($buttonsAliac as $key => $item) {
            $this->buttons[$item] = \array_search($key, $arButton) !== false;
        }

        return $this;
    }

    public function showButton(): void
    {
        $this->tabControl->Buttons($this->buttons ?: $this->setButton()->buttons);
    }

    /**
     * Конец формы с вкладками
     *
     * @return self
     */
    public function end(): self
    {
        $this->tabControl->End();

        return $this;
    }
}
