<?php

declare(strict_types=1);

namespace Initstudio\Core\Admin;

class Button
{
    public const SAVE = 0;
    public const APPLY = 1;
    public const CANCEL = 2;
    public const SAVE_AND_ADD = 4;
}
