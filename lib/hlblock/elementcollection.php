<?php

declare(strict_types=1);

namespace Initstudio\Core\HLBlock;

/**
 * Набор элементов HL-блока
 *
 * @author Bondar Maksim <max@initstudio.ru>
 *
 * @copyright 2021 InitStudio
 *
 * @package Initstudio\Core\HLBlock
 */
class ElementCollection
{
    protected string $className;
    protected HLElementParams $params;
    protected array $result;
    protected HLBlock $hlBlock;

    /**
     * @var Initstudio\Core\HLBlock\Element[]
     */
    protected array $collection = [];
    private array $keyId = [];
    private array $keyXmlId = [];

    public function __construct(HLElementParams $params, HLBlock $hlBlock)
    {
        $this->className = $hlBlock->getClass();
        $this->params = $params;
        $this->hlBlock = $hlBlock;
    }

    protected function getArParams(): array
    {
        $params = [];
        $params['select'] = $this->params->getSelect();

        if ($this->params->getFilter()) {
            $params['filter'] = $this->params->getFilter();
        }
        if ($this->params->getGroup()) {
            $params['group'] = $this->params->getGroup();
        }
        if ($this->params->getOrder()) {
            $params['order'] = $this->params->getOrder();
        }
        if ($this->params->getLimit()) {
            $params['limit'] = $this->params->getLimit();
        }
        if ($this->params->getOffset()) {
            $params['offset'] = $this->params->getOffset();
        }

        return $params;
    }

    protected function fetch()
    {
        $this->result = $this->className::getList($this->getArParams())->fetchAll();
        foreach ($this->result as $item) {
            $element = $this->hlBlock->add()->setFields($item);
            $count = count($this->collection);
            $this->collection[$count] = $element;
            if (!empty($item['ID'])) {
                $this->keyId[$item['ID']] = &$this->collection[$count];
            }
            if (!empty($item['UF_XML_ID'])) {
                $this->keyXmlId[$item['UF_XML_ID']] = &$this->collection[$count];
            }
        }

        return $this;
    }

    public function getList(): ?array
    {
        return $this->fetch()->result ?: null;
    }

    public function getRow(): ?array
    {
        $this->params->setLimit(1);

        return $this->fetch()->result[0] ?: null;
    }

    public function byId(int $id): ?Element
    {
        return $this->fetch()->keyId[$id] ?: null;
    }

    public function byXmlId(string $id): ?Element
    {
        return $this->fetch()->keyXmlId[$id] ?: null;
    }
}
