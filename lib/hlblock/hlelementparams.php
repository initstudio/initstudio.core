<?php

declare(strict_types=1);

namespace Initstudio\Core\HLBlock;

use Initstudio\Core\Collection\Params;

/**
 * Параметры для выборки из HL
 *
 * @author Bondar Maksim <max@initstudio.ru>
 *
 * @copyright 2021 InitStudio
 *
 * @package Initstudio\Core\HLBlock
 */
class HLElementParams extends Params
{
}
