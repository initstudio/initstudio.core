<?php

declare(strict_types=1);

namespace Initstudio\Core\HLBlock;

use Bitrix\Highloadblock\HighloadBlockTable;
use Bitrix\Main\Loader;
use Initstudio\Core\Text\StringHelper;

/**
 * Абстрактный класс HL-блока
 * 
 * @method string getClass()
 *
 * @author Bondar Maksim <max@initstudio.ru>
 *
 * @copyright 2021 InitStudio
 *
 * @package Initstudio\Core\HLBlock
 */
abstract class HLBlock
{
    protected const NAME = '';
    protected $entity;
    protected $class;

    protected static  array $instance = [];

    abstract public function add(): Element;
    abstract public function getFields(): array;

    protected function __construct()
    {
        if (Loader::includeModule('highloadblock')) {
            $name = $this->getName();
            $this->entity = HighloadBlockTable::compileEntity($name);
            $this->class = $this->entity->getDataClass();
        }
    }

    /**
     * Возвращает инстанс объекта
     *
     * @return static
     */
    public static function getInstance()
    {
        $code = static::NAME;
        if (empty(static::$instance[$code])) {
            static::$instance[$code] = new static();
        }

        return static::$instance[$code];
    }

    public function getElements(HLElementParams $params): ElementCollection
    {
        $elements = new ElementCollection($params, static::$instance[$this->getName()]);

        return $elements;
    }

    public function getName(): string
    {
        return static::NAME;
    }

    public function __call($name, $arguments)
    {
        if (\strpos($name, 'set') === 0) {
            $var = StringHelper::snakeToCamel(\str_replace('set', '', $name));
            if (property_exists($this, $var)) {
                $this->$var = $arguments[0];
            }
        } elseif (\strpos($name, 'get') === 0) {
            $var = StringHelper::snakeToCamel(\str_replace('get', '', $name));
            if (property_exists($this, $var)) {
                return $this->$var;
            }
        }

        return $this;
    }
}
