<?php

declare(strict_types=1);

namespace Initstudio\Core\HLBlock;

use Initstudio\Core\Text\StringHelper;

/**
 * Элемент HL-блока
 *
 * @author Bondar Maksim <max@initstudio.ru>
 *
 * @copyright 2021 InitStudio
 *
 * @package Initstudio\Core\HLBlock
 */
class Element extends \stdClass
{
    private HLBlock $hl;

    public function __construct(HLBlock $hl)
    {
        $this->hl = $hl;
    }

    public function __call($name, $arguments)
    {
        $fields = $this->hl->getFields();
        if (\strpos($name, 'set') === 0) {
            $var = \str_replace('set', '', $name);
            // if (\array_key_exists($var, $fields)) {
            $this->{$var} = $arguments[0];
            // }
        } elseif (\strpos($name, 'get') === 0) {
            $var = \str_replace('get', '', $name);
            if (\array_key_exists($var, $fields) && property_exists($this, $var)) {
                return $this->$var;
            }
        }

        return $this;
    }

    public function setFields(array $fieldList): self
    {
        // $HLFields = $this->hl->getFields();
        foreach ($fieldList as $code => $value) {
            $this->$code = $value;
        }

        return $this;
    }

    public function save()
    {
        $class = $this->hl->getClass();
        $fields = [];
        foreach ($this->hl->getFields() as $key => $data) {
            if (property_exists($this, $key)) {
                $fields[$key] = $this->{$key};
            }
        }
        $result = $class::add($fields);
        return $result;
    }
}
