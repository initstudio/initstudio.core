<?php

declare(strict_types=1);

namespace Initstudio\Core\Image;

use Bitrix\Main\IO\Path;

\define('INIT_WEBP_SERIZE_PROPORTIONATELY', 0);
\define('INIT_WEBP_SERIZE_CROP', 1);
\define('INIT_WEBP_SERIZE_CROP_HORIZONTALLY', 2);
\define('INIT_WEBP_SERIZE_CROP_VERTICALLY', 3);

class Webp
{
    private $id;
    private $originalPath;
    private $originWidth;
    private $originHeight;
    private $newWidth;
    private $newHeight;
    private $newFilePath;
    private $name;
    private $scale = 85;
    private $watemark = null;


    public function __construct($src)
    {
        $fileArray = $this->getFileArray($src);

        if ($fileArray) {
            $this->originWidth = (int)$fileArray['WIDTH'];
            $this->originHeight = (int)$fileArray['HEIGHT'];
            $this->originalPath = \file_exists($_SERVER['DOCUMENT_ROOT'] . $fileArray['SRC'])
                ? $_SERVER['DOCUMENT_ROOT'] . $fileArray['SRC']
                : null;
            [$this->name] = explode('.', $fileArray['FILE_NAME']);
        }
    }

    protected function getFileArray($src): ?array
    {
        $fileArray = null;
        if (
            \is_array($src)
            && isset($src['ID'])
            && isset($src['WIDTH'])
            && isset($src['HEIGHT'])
            && isset($src['SRC'])
            && isset($src['FILE_NAME'])
        ) {
            $fileArray = $src;
        } else if (\is_int($src) || \is_string($src) || isset($src['ID'])) {
            $this->id = $src['ID'] ?? $src;
            $fileArray = \CFile::GetFileArray($this->id);
        }

        return $fileArray;
    }

    /**
     * Создаёт экземпляр класса картинки
     *
     * @param int|array $src id из таблицы файлов или массив, описывающий файл
     *
     * @return self
     */
    // public static function create($src): self
    // {
    //     if (!$src) {
    //         return null;
    //     }
    //     $img = new static($src);

    //     return $img;
    // }

    // public function resize(float $width, float $height,)
    // {
    // }

    /**
     * Конвертирует картинку в webp и возвращает массив с данными
     *
     * @param integer|string|array $src принимает id файла,
     * относительную ссылку на файл, массив из метода \CFile::GetFileArray
     * @param array $params
     * [
     *  'proportions' - тип ресайза,
     *  'size' - новые размеры,
     *  'scale' - качество,
     *  'watemark' - водяной знак = [
     *      'src' - путь к картинке,
     *      'transparency' - прозрачность,
     *      'size' - размеры относительно конечного результата (1 = 100%)
     *  ]
     * ]
     *
     * @return array
     */
    public static function get($src, $params = []): ?array
    {
        if (!$src) {
            return null;
        }
        $img = new static($src);

        if (!$img->originalPath) {
            return null;
        }

        if (isset($params['proportions'])) {
            $proportions = $params['proportions'];
        } else {
            $proportions = INIT_WEBP_SERIZE_PROPORTIONATELY;
        }

        if (isset($params['size'])) {
            $img->setNewSize($params['size'], $proportions);
        } else {
            $img->setNewSize();
        }

        if (isset($params['scale'])) {
            $img->scale = (int)$params['scale'];
        }

        if (isset($params['watemark'])) {
            $img->watemark = $params['watemark'];
        }

        $img->createImage();

        return [
            'width' => $img->newWidth,
            'height' => $img->newHeight,
            'src' => $img->newFilePath
        ];
    }


    /**
     * Возвращает полный путь к картинке
     *
     * @param integer|string|array $src
     *
     * @return string
     */
    // private function getPath($src): string
    // {
    //     $srcType = \gettype($src);

    //     $path = '';
    //     if ($srcType == 'integer' || ($srcType == 'string' && (int)$src > 0)) {
    //         $path = $_SERVER['DOCUMENT_ROOT'] . \CFile::GetPath($src);
    //     } else if ($srcType == 'string' && (int)$srcType === 0) {
    //         if (\file_exists($_SERVER['DOCUMENT_ROOT'] . $src)) {
    //             $path = $_SERVER['DOCUMENT_ROOT'] . $src;
    //         }
    //     } else if ($srcType == 'array') {
    //         $path = $_SERVER['DOCUMENT_ROOT'] . $src['SRC'];
    //     }

    //     return $path;
    // }

    protected function createImage()
    {
        $this->newFilePath =  $this->getDir() . '/' . $this->name . '.webp';

        if (!$this->checkFileExists()) {
            //создаем картинку под размеры
            $image_p = imagecreatetruecolor($this->newWidth, $this->newHeight);
            $extension = Path::getExtension($this->originalPath);

            //создадим новое изображение
            switch (strtolower($extension)) {
                case 'png':
                    $im = imagecreatefrompng($this->originalPath);
                    break;
                case 'jpeg':
                    $im = imagecreatefromjpeg($this->originalPath);
                    break;
                case 'jpg':
                    $im = imagecreatefromjpeg($this->originalPath);
                    break;
                case 'bmp':
                    $im = imagecreatefrombmp($this->originalPath);
                    break;
                case 'tiff':
                    $im = imagecreatefromgif($this->originalPath);
                    break;
            }

            //сохраняем прозрачность
            imageAlphaBlending($image_p, false);
            imageSaveAlpha($image_p, true);

            if ($im) {
                imagecopyresampled(
                    $image_p,
                    $im,
                    0,
                    0,
                    0,
                    0,
                    $this->newWidth,
                    $this->newHeight,
                    $this->originWidth,
                    $this->originHeight
                );

                $this->setWatemark($image_p);

                //запишем новое изображение
                imagewebp($image_p, $_SERVER['DOCUMENT_ROOT'] . $this->newFilePath, $this->scale);
            }
        }
    }

    private function setNewSize(array $size = [], $proportional = INIT_WEBP_SERIZE_PROPORTIONATELY)
    {
        switch ($proportional) {
            case INIT_WEBP_SERIZE_PROPORTIONATELY:
                $this->proportionalResize($size);
                break;

            case INIT_WEBP_SERIZE_CROP:
                $this->cropResize($size, INIT_WEBP_SERIZE_CROP);
                break;

            case INIT_WEBP_SERIZE_CROP_HORIZONTALLY:
                $this->cropResize($size, INIT_WEBP_SERIZE_CROP_HORIZONTALLY);
                break;

            case INIT_WEBP_SERIZE_CROP_VERTICALLY:
                $this->cropResize($size, INIT_WEBP_SERIZE_CROP_VERTICALLY);
                break;

            default:
                $this->proportionalResize($size);
                break;
        }
    }

    private function proportionalResize(array $size = [])
    {
        $height = (int)$size['height'] ?? 0;
        $width = (int)$size['width'] ?? 0;
        if (
            (empty($height)
                && empty($width))
            || ($height == $this->originHeight
                && $width == $this->originWidth)
        ) {
            $this->newHeight = $this->originHeight;
            $this->newWidth = $this->originWidth;
        } else {
            $wRate = $width / $this->originWidth;
            $hRate = $height / $this->originHeight;

            if ($wRate > $hRate) {
                $this->newHeight = $height;
                $this->newWidth = (int)round($this->originWidth * $hRate);
            } else {
                $this->newWidth = $width;
                $this->newHeight = (int)round($this->originHeight * $wRate);
            }
        }
    }

    private function cropResize(array $size = [], $type)
    {
        if ($type == INIT_WEBP_SERIZE_CROP_HORIZONTALLY) {
            $this->newHeight = $size['height'];
            $this->newWidth = $size['width'];
        }

        if ($type == INIT_WEBP_SERIZE_CROP_VERTICALLY) {
        }
    }

    protected function getDir()
    {
        return $this->checkDir(
            '/upload/webp/' .
                $this->newWidth . 'X' .
                $this->newHeight . '/' .
                substr($this->name, 0, 3)
        );
    }

    protected function checkDir(string $dirPath): string
    {
        $dirs = explode('/', $dirPath);
        $path = '';
        foreach ($dirs as $dir) {
            if ($dir != '') {
                $path .= '/' . $dir;
                if (!is_dir($_SERVER['DOCUMENT_ROOT'] . $path)) {
                    $this->createNewDir($path);
                }
            }
        }
        return $dirPath;
    }

    protected function createNewDir(string $dirPath): void
    {
        mkdir($_SERVER['DOCUMENT_ROOT'] . $dirPath);
    }

    protected function checkFileExists()
    {
        return \file_exists($_SERVER['DOCUMENT_ROOT'] . $this->newFilePath);
    }

    protected function setWatemark(&$image_p): void
    {
        //сохраняем прозрачность
        imageAlphaBlending($image_p, true);
        imageSaveAlpha($image_p, true);
        ['src' => $src] = $this->watemark;
        if ($src && \is_file($src)) {
            $extension = Path::getExtension($src);
            $transparency = $this->watemark['transparency'] ?: 80;
            $size = $this->watemark['size'] ?: 0.8;

            switch ($extension) {
                case 'png':
                    $watermark = imagecreatefrompng($src);
                    break;
                case 'jpeg':
                    $watermark = imagecreatefromjpeg($src);
                    break;
                case 'jpg':
                    $watermark = imagecreatefromjpeg($src);
                    break;
                case 'bmp':
                    $watermark = imagecreatefrombmp($src);
                    break;
                case 'tiff':
                    $watermark = imagecreatefromgif($src);
                    break;
            }
            // Исходные размеры водяного знака
            $watermarkWidth = imagesx($watermark);
            $watermarkHeight = imagesy($watermark);


            // Новые размеры водняного знака
            $wRate =  ($this->newWidth * $size) / $watermarkWidth;
            $hRate = ($this->newHeight * $size) / $watermarkHeight;

            if ($wRate < $hRate) {
                $stampWidth = (int)round($this->newWidth * $size);
                $stampHeight =  (int)round($wRate * $watermarkHeight);
            } else {
                $stampHeight = (int)round($this->newHeight * $size);
                $stampWidth =  (int)round($hRate * $watermarkWidth);
            }

            $x = (int)round(($this->newWidth - $stampWidth) / 2);
            $y = (int)round(($this->newHeight - $stampHeight) / 2);

            //Ресайзим водяной знак
            imagecopyresampled(
                $image_p,
                $watermark,
                $x,
                $y,
                0,
                0,
                $stampWidth,
                $stampHeight,
                $watermarkWidth,
                $watermarkHeight
            );
        }
    }
}
