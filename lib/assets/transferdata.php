<?php

declare(strict_types=1);

namespace Initstudio\Core\Assets;

/**
 * Выводит на странице js данные из php
 *
 * @author Bondar Maksim <max@initstudio.ru>
 *
 * @copyright 2021 InitStudio
 *
 * @package InitStudio\Core\Assets
 */
class TransferData
{
    /**
     * Вставляет переменную js в код страницы
     *
     * @param string $varName
     * @param $data
     *
     * @return void
     */
    public static function pasteJs(string $varName, $data = null)
    {
        $print = static::getPasteType($varName, $data) . (static::parseVar($data) ?: '');
        echo static::getScriptTag($print);
    }

    /**
     * Создает новую js переменную или присваивает значение старой
     *
     * @param string $varName
     * @param string $data
     *
     * @return string
     */
    protected static function getPasteType($varName, $data)
    {
        if (empty($data)) {
            return $varName;
        } else {
            return "$varName = ";
        }
    }

    /**
     * Обрабатывает данные в зависимости от типа данных
     *
     * @param mixed $data
     *
     * @return mixed
     */
    protected static function parseVar($data)
    {
        switch (\gettype($data)) {
            case 'array':
                return \json_encode($data, JSON_UNESCAPED_UNICODE);
                break;

            case 'string':
                return "'$data'";
                break;

            case 'integer':
            case 'double':
            case 'boolean':
                return $data;
                break;

            case 'object':
                return "'$data'";
                break;
        }
    }

    /**
     * Оборачивает полученные данные в тег <script>
     *
     * @param mixed $data
     *
     * @return string
     */
    protected static function getScriptTag($data): string
    {
        return "<script>$data</script>";
    }
}
