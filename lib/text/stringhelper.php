<?php

declare(strict_types=1);

namespace Initstudio\Core\Text;

class StringHelper
{
    public static function snakeToCamel(string $input)
    {
        return lcfirst(str_replace(' ', '', ucwords(str_replace('_', ' ', $input))));
    }
}
