<?php

declare(strict_types=1);

namespace Initstudio\Core\Ajax;

use Bitrix\Main\Application;
use Bitrix\Main\Web\Uri;

/**
 * Класс для фоновых запросов
 *
 * @author Bondar Maksim <max@initstudio.ru>
 *
 * @copyright 2021 InitStudio
 *
 * @package Initstudio\Core\Ajax
 */
class BackgroundLoad
{
    /**
     * Ключ в массиве $_POST, который указывает, что хит выполняется фоне
     * @var string
     */
    protected static $postKeyBgLoad = 'backgroundDownload';

    /**
     * Определяет тип запроса
     * @return bool
     */
    public static function isBgLoad(): bool
    {
        return static::isParam([
            static::$postKeyBgLoad => 'Y'
        ]);
    }

    /**
     * Проверяет адрес страницы на соответсвие переданному аргументу
     *
     * @param string $url
     *
     * @return bool
     */
    public static function isPage(string $url): bool
    {
        $uriString = Application::getInstance()
            ->getContext()
            ->getRequest()
            ->getRequestUri();
        $path = (new Uri($uriString))->getPath();
        $result = $path === $url;

        return $result;
    }

    /**
     * Проверяет наличие параметра и его значение
     *
     * @param array|string $param
     *
     * @return bool
     */
    public static function isParam($param): bool
    {
        $request = Application::getInstance()->getContext()->getRequest();

        if (\is_array($param)) {
            $paramString = \array_key_first($param);
            $value = $param[$paramString];
        } else {
            $paramString = $param;
        }

        $res = $request->getPost($paramString) ?? $request->getQuery($paramString) ?? false;

        if (\is_set($value)) {
            $res = $value === $res;
        }

        return (\boolval($res));
    }
}
