<?php

declare(strict_types=1);

namespace Initstudio\Core;

/**
 * Для описания инструментов
 *
 * @author Bondar Maksim <max@initstudio.ru>
 *
 * @copyright 2021 InitStudio
 *
 * @package InitStudio
 */
interface IBase
{
    /**
     * Возвращает список обработчиков событий
     * @return array [moduleName=>eventName]
     */
    public static function getEventList(): array;
}
