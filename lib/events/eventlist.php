<?php

declare(strict_types=1);

namespace Initstudio\Core\Events;

use Initstudio\Core\IBlock\IBlock;
use Initstudio\Core\Template\TemplateLoader;

class EventList
{
    const EVENTS = [
        IBlock::class,
        TemplateLoader::class
    ];
}
