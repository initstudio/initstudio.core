<?php

declare(strict_types=1);

namespace Initstudio\Core\Events;

/**
 * Собирает и устанавливает обработчики событий
 *
 * @author Bondar Maksim <max@initstudio.ru>
 *
 * @copyright 2021 InitStudio
 *
 * @package InitStudio\Core
 */
class EventManager
{
    /**
     * Собирает и устанавливает обработчики событий
     *
     * @param array $classArray
     *
     * @return void
     */
    public static function initAutoloadEvents(array $classArray): void
    {
        foreach ($classArray as $class) {
            if (class_exists($class) && method_exists($class, 'getEventList')) {
                $eventList = $class::getEventList();
                foreach ($eventList as  $eventName => $moduleName) {
                    $function = static::getFunctionName($eventName);
                    if (method_exists($class, $function)) {
                        \AddEventHandler($moduleName, $eventName, [$class, $function], 1);
                    }
                }
            }
        }
    }

    /**
     * Возвращает имя обработчика
     *
     * @param string $eventName
     *
     * @return string
     */
    protected static function getFunctionName(string $eventName): string
    {
        return lcfirst($eventName) . 'EventHandler';
    }

    /**
     * Проверяет и вызывает обработчики у родительского класса
     *
     * @param string $class
     * @param string $method
     *
     * @return mixed
     */
    public static function checkParentEventHandler(
        string $class,
        string $method,
        array $params = []
    ) {
        if (class_exists($class)) {
            if (method_exists($class, $method)) {
                return $class::$method(...$params);
            }
        }
        return false;
    }
}
