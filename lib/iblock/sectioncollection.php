<?php

declare(strict_types=1);

namespace Initstudio\Core\IBlock;

class SectionCollection extends CollectionBase
{

    public function __construct(
        IBlock &$iblock,
        ?SectionParams $params
    ) {
        $this->iblock = $iblock;
        // $this->setParams($params);
        if ($params) {
            $this->params = $params;
        }
    }

    /**
     * Устанавливает объект выборки
     * 
     * @return CIBlockResult 
     */
    private function getGetListResult(): \CIBlockResult
    {
        $res = \CIBlockSection::GetList(
            $this->params->getOrder(),
            $this->params->getFilter(),
            false, //$this->params['bIncCnt'],
            $this->params->getSelect(),
            $this->params->getLimit()
        );

        return $res;
    }

    /**
     * Возвращает результаты выборки списка
     * 
     * @return array 
     */
    public function getList(): array
    {
        $res = self::fetchAll($this->getGetListResult());
        // $link = [];
        foreach ($res as $key => $section) {
            $this->addItem(new Section($section, $this->iblock));
            // $link[$element['ID']] = &$res[$key];
        }
        // if (isset($this->params['property'])) {
        //     $propertyRes = $this->getPropertyRaw();
        //     foreach ($propertyRes as $elementId => $propertyValue) {
        //         $link[$elementId]['PROPERTY'] = $propertyValue;
        //     }
        // }
        return $res;
    }

    /**
     * Возвращает результаты выборки строки
     * 
     * @return array|null
     */
    public function getRow(): ?array
    {
        $res = $this->getGetListResult()->Fetch() ?: null;

        if ($res) {
            $this->addItem(new Section($res, $this->iblock));
        }

        // if (isset($this->params['property'])) {
        //     $propertyRes = $this->getPropertyRaw();
        //     $res['PROPERTY'] = $propertyRes[$res['ID']];
        // }
        return $res;
    }

    /**
     * Возвращает поля разделов из базы
     *
     * @param ElementCollection $elementCollection
     *
     * @return array
     */
    public function getElementSection(ElementCollection $elementCollection): array
    {
        $rs = \CIBlockElement::GetElementGroups($elementCollection->getId());
        $sectionList = [];
        foreach ($this->fetchAll($rs) as $section) {
            $sectionList[$section['IBLOCK_ELEMENT_ID']][] = $section;
            $this->addItem(new Section($section, $this->iblock));
        }

        return $sectionList;
    }
}
