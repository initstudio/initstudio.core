<?php

declare(strict_types=1);

namespace Initstudio\Core\IBlock;

/**
 * Коллекция свойств инфоблока
 *
 * @author Bondar Maksim <max@initstudio.ru>
 *
 * @copyright 2021 InitStudio
 *
 * @package Initstudio\Core\IBlock
 */
class PropertyCollection extends CollectionBase
{
    use PropertyTrait;

    /**
     * Массив объектов свойств
     * 
     * @var Property[]
     */
    protected $propertyList;

    /**
     * @param array<int,array> $propertyList
     * @param IBlock|IBlockCollection $iblock
     */
    public function __construct(array $propertyList, &$iblock)
    {
        $this->iblock = $iblock;
        foreach ($propertyList as $property) {
            $prop = new Property($property, $iblock);
            $this->collection[] = $prop;
            $this->setCode($prop)->setId($prop);
        }
    }

    /**
     * Устанавливает парамаетры для выборки
     * 
     * @param array $params 
     * 
     * @return $this 
     */
    private function setParams(array $params): self
    {
        // $this->params['order'] = $params['order'] ?? ['SORT' => 'ASC'];
        // $this->params['filter'] = $this->getFilter($params['filter'] ?? []);
        // $this->params['group'] = $params['group'] ?? false;
        // $this->params['limit'] = $this->getLimit($params);
        // $this->params['select'] = $this->getSelect($params['select'] ?? []);
        return $this;
    }

    /**
     * Устанавливает объект выборки
     * 
     * @return \Initstudio\Core\IBlock\PropertyCollection 
     * @throws \Bitrix\Main\Config\ConfigurationException 
     * @throws \Bitrix\Main\LoaderException 
     * @throws \Bitrix\Main\ObjectNotFoundException 
     * @throws \Bitrix\Main\ArgumentNullException 
     */
    private function setResult(): self
    {
        $this->res = \CIBlockProperty::GetList(
            $this->params['order'],
            $this->params['filter']
        );

        return $this;
    }
}
