<?php

declare(strict_types=1);

namespace Initstudio\Core\IBlock;

interface IBase
{
    public function getCode();
}
