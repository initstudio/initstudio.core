<?php

declare(strict_types=1);

namespace Initstudio\Core\IBlock;

/**
 * Класс для работы с множеством инфоблоков
 *
 * @author Bondar Maksim <max@initstudio.ru>
 *
 * @copyright 2021 InitStudio
 *
 * @package Initstudio\Core\IBlock
 */
class IBlockCollection extends CollectionBase
{
    use IBlockTrait;

    /**
     * @param Array<string>|IBlock[]|null $iblockList список инфоблоков
     */
    public function __construct(?array $iblockList = null)
    {
        $this->propertyList = new PropertyCollection([], $this);
        foreach ($iblockList as $iblock) {
            if (\class_exists($iblock)) {
                $this->addItem($iblock::getInstance());
                $this->setType($iblock::getInstance());
            }
        }
    }

    /**
     * Возварщает список типов инфоблока
     * @return array<int,string> 
     */
    public function getType(): array
    {
        return $this->type;
    }

    /**
     * Set тип инфоблока
     * @param  IBlock  Тип инфоблока
     * @return  self
     */
    protected function setType(IBlock $item): self
    {
        $this->type[$item->getId()] = $item->getType();

        return $this;
    }
}
