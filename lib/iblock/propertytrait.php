<?php

declare(strict_types=1);

namespace Initstudio\Core\IBlock;

/**
 * Набор для работы со свойствами
 *
 * @author Bondar Maksim <max@initstudio.ru>
 *
 * @copyright 2021 InitStudio
 *
 * @package Initstudio\Core\IBlock
 */
trait PropertyTrait
{
    /**
     * Ссылка на экземпляр инфоблока
     * @var IBlock|IBlockCollection
     */
    // private $iblock;

    /**
     * Возвращает значение свойства по id элемента
     * @param mixed $elementId 
     * @return null|array 
     * @throws \Bitrix\Main\ArgumentException 
     * @throws \Bitrix\Main\SystemException 
     * @throws \Bitrix\Main\ObjectPropertyException 
     * @throws \Bitrix\Main\LoaderException 
     * @throws \Bitrix\Main\ArgumentOutOfRangeException 
     */
    public function getValue($elementId): ?array
    {
        $res = [];
        \CIBlockElement::GetPropertyValuesArray(
            $res,
            $this->iblock->getId(),
            ['ID' => $elementId],
            ['ID' => $this->id]
        );

        return $res;
    }
}
