<?php

declare(strict_types=1);

namespace Initstudio\Core\IBlock;

use \Bitrix\Main\SystemException;

class Section extends Base
{
    /**
     * Родительский раздел
     * @var Section|null
     */
    private $parrent = null;

    /**
     * Флаг наличия изменений
     * @var boolean
     */
    private $isUpdated = false;

    public function __construct(array $data, IBlock &$iblock)
    {
        $this->iblock = $iblock;
        $this->setFields($data);

        if (!empty($data['IBLOCK_SECTION_ID'])) {
            $this->setParrent($this->iblock->getSection()->byId((int)$data['IBLOCK_SECTION_ID']));
        }

        $this->isUpdated = false;
    }

    /**
     * Устанавливает параметры раздела
     * @param array $data 
     * @return \Initstudio\Core\IBlock\Section 
     */
    private function setFields(array $data): self
    {
        $this->fields = $data;
        $this->fields['IBLOCK_ID'] = $this->iblock->getId();

        isset($data['CODE']) ? $this->setCode($data['CODE']) : '';
        isset($data['ID']) ? $this->setId((int)$data['ID']) : '';

        return $this;
    }

    /**
     * Get родительский раздел
     *
     * @return  Section|null
     */
    public function getParrent()
    {
        return $this->parrent;
    }

    /**
     * Set родительский раздел
     *
     * @param  Section|null  $parrent  Родительский раздел
     *
     * @return  self
     */
    public function setParrent($parrent)
    {
        $this->parrent = $parrent;
        $this->isUpdated = true;
        return $this;
    }

    public function setCode(string $code): self
    {
        parent::setCode($code);
        $this->isUpdated = true;

        return $this;
    }

    // public function getNavChain()
    // {
    //     $nav = \CIBlockSection::GetNavChain(
    //         $this->iblock->getId(),
    //         $this->id
    //     );
    //     while ($arSectionPath = $nav->Fetch()) {
    //     }
    // }

    /**
     * Создаёт новый раздел и возвращает его объект
     * 
     * @param \Initstudio\Core\IBlock\IBlock $iblock экземпляр инфоблока
     * 
     * @return null|\Initstudio\Core\IBlock\Section 
     */
    public function save(): self
    {
        $bs = new \CIBlockSection;

        if ($this->getId()) {
            $res = $bs->Update($this->getId(), $this->getFields());
            $this->isUpdated = false;
        } else {
            $ID = $bs->Add($this->getFields());
            if ($ID > 0) {
                $this->setId($ID);
                $this->isUpdated = false;
            }
        }
        return $this;
    }

    /**
     * Копирует раздел в указанный инфоблок с озменёнными параметрами
     * @param \Initstudio\Core\IBlock\IBlock $iblock 
     * @param array $fields 
     * @return null|\Initstudio\Core\IBlock\Section 
     */
    public function copy(IBlock &$iblock, array $fields = []): ?Section
    {
        $parent = $this->getParrent();
        if ($parent && empty($fields['IBLOCK_SECTION_ID'])) {
            ['ID' => $fields['IBLOCK_SECTION_ID']] = $iblock::getSection([
                'select' => ['ID'],
                'filter' => ['=CODE' => $parent->getCode()]
            ])->getRow();
        }
        $newFields = \array_merge_recursive($this->getFields(), $fields);
        unset($newFields['ID']);
        return (new Section($newFields, $iblock))->save();
    }
}
