<?php

declare(strict_types=1);

namespace Initstudio\Core\IBlock;

use Initstudio\Core\Collection\Params;

/**
 * Объект параметров для выборки элементов
 *
 * @author Bondar Maksim <max@initstudio.ru>
 *
 * @copyright 2021 InitStudio
 *
 * @package InitStudio
 */
class ElementParams extends Params
{
    /**
     * ID инфоблока
     *
     * @var IBlock|null
     */
    private $iblock = null;

    private array $selectProperty = [];

    /**
     * @param string|IBlock $iblock
     */
    public function __construct($iblock)
    {
        $this->iblock = $iblock::getInstance();
    }

    public function getLimit()
    {
        $res = null;
        if (!empty($this->limit) && empty($this->offset)) {
            $res = ['nTopCount' => $this->limit];
        } elseif (isset($this->limit) && isset($this->offset)) {
            $res = [
                'iNumPage' => $this->offset / $this->limit + 1,
                'nPageSize' => $this->limit
            ];
        }

        return $res;
    }

    public function setFilter(array $filter): self
    {
        $property = $filter['property'] ?? null;
        unset($filter['property']);
        $this->filter = $filter;

        if ($property) {
            $this->setFilterProperty($property);
        }

        return $this;
    }

    /**
     * Возвращает фильтр для выборки
     * 
     * @return array 
     */
    public function getFilter(): array
    {
        $res = \array_merge(
            $this->filter,
            ['IBLOCK_ID' => $this->iblock::getId()]
        );

        return $res;
    }

    public function setFilterProperty(array $property): self
    {
        foreach ($property as $code => $value) {
            $newCode = $this->getPropertyKeyForSearch($code);
            $this->filter[$newCode] = $value;
        }

        return $this;
    }

    public function setSelect(array $select): self
    {
        $property = $select['property'] ?? null;
        unset($select['property']);
        $this->select = count($select) ? ['ID', 'CODE', ...$select] : [];

        if ($property) {
            $this->setSelectProperty($property);
        }

        return $this;
    }

    public function setSelectProperty(array $property): self
    {
        foreach ($property as $code) {
            if ($code === '*') {
                $this->selectProperty = [];
            } else {
                $this->selectProperty[] = $code; //$this->iblock->getProperty()->byCode($code)->getId();
            }
        }

        return $this;
    }

    public function getSelectProperty(): array
    {
        return $this->selectProperty;
    }

    private function getPropertyKeyForSearch(string $code): string
    {
        [
            'FIELD' => $property,
            'OPERATION' => $operationName
        ] = \CAllSQLWhere::MakeOperation($code);

        $type = $this->iblock
            ->getProperty()
            ->byCode($property)
            ->getType();

        $key = '';
        $defaultOperation = '';

        switch ($type) {
            case 'N':
            case 'E':
            case 'G':
            default:
                $key = 'PROPERTY_' . $property;
                break;

            case 'S':
                $key = 'PROPERTY_' . $property;
                // $defaultOperation = '=';
                break;

            case 'L':
                $key = 'PROPERTY_' . $property . '_VALUE';
                $defaultOperation = '=';
                break;
        }
        $operation = array_search($operationName, \CAllSQLWhere::$operations) ?: $defaultOperation;

        return $operation . $key;
    }
}
