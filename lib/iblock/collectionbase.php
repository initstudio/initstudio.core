<?php

declare(strict_types=1);

namespace Initstudio\Core\IBlock;

use Initstudio\Core\Collection\Params;
use \Initstudio\Core\IBlock\IBlock;
use \Initstudio\Core\IBlock\IBlockCollection;

/**
 * Абстрактный класс коллекции
 * 
 * @property int[]|null $id
 * @property string[]|null $code
 * @property IBlock|IBlockCollection|null $iblock
 *
 * @author Bondar Maksim <max@initstudio.ru>
 *
 * @copyright 2021 InitStudio
 *
 * @package Initstudio\Core\IBlock
 */
abstract class CollectionBase extends Base
{
    /**
     * Объект выборки
     * 
     * @var \CIBlockResult
     */
    protected $res;

    /**
     * Параметры для выборки
     * 
     * @var Initstudio\Core\Collection\Params|null
     */
    protected $params = null;

    /**
     * Коллекция
     * 
     * @var array
     */
    protected $collection;

    /**
     * Добавляет элемент в коллекцию
     * 
     * @param mixed $item
     * 
     * @return self
     */
    public function addItem(&$item): self
    {
        $count = count($this->collection);
        $this->collection[$count] = $item;
        $this->setCode($this->collection[$count])
            ->setId($this->collection[$count]);

        return $this;
    }

    /**
     * Добавляет идентификатор в коллецию
     * 
     * @param mixed $item
     * 
     * @return self
     */
    protected function setId(&$item): self
    {
        $this->id[$item->getId()] = $item;

        return $this;
    }

    /**
     * Возвращает список идентификаторов
     * 
     * @return array
     */
    public function getId(): array
    {
        return \array_keys($this->id ?? []);
    }

    /**
     * Добавляет символьный код свойства в коллецию
     * 
     * @param IBlock $item
     * 
     * @return self
     */
    protected function setCode(&$item): self
    {
        $this->code[$item->getCode()] = $item;

        return $this;
    }

    /**
     * Возвращает список символьных кодов
     * @return array
     */
    public function getCode()
    {
        return \array_keys($this->code ?? []);
    }

    /**
     * Возвращает экземпляр коллекции по идентификатору
     * 
     * @param int|int[] $id 
     * 
     * @return mixed|null
     */
    public function byId($id)
    {
        [$id => $item] = $this->id;

        return $item;
    }

    /**
     * Возвращает экземпляр коллекции по символьному коду
     * 
     * @param string|string[] $code
     * 
     * @return mixed|null
     */
    public function byCode($code)
    {
        [$code => $item] = $this->code;

        return $item;
    }
}
