<?php

declare(strict_types=1);

namespace Initstudio\Core\IBlock;

/**
 * Набор для классов с инфоблоками
 *
 * @author Bondar Maksim <max@initstudio.ru>
 *
 * @copyright 2021 InitStudio
 *
 * @package Initstudio\Core\IBlock
 */
trait IBlockTrait
{
    /**
     * Коллекция свойств
     * 
     * @var PropertyCollection
     */
    protected $propertyList;

    /**
     * Тип инфоблока
     *
     * @var string|string[]|null
     */
    private $type = null;

    /**
     * Возвращает список свойств инфоблока
     * 
     * @return array 
     * @throws \Bitrix\Main\Config\ConfigurationException 
     * @throws \Bitrix\Main\LoaderException 
     * @throws \Bitrix\Main\ObjectNotFoundException 
     * @throws \Bitrix\Main\ArgumentNullException 
     */
    protected function getPropertyRaw(): array
    {
        $properties = \CIBlockProperty::GetList(
            [],
            ["IBLOCK_CODE" => $this->getCode()]
        );

        return static::fetchAll($properties);
    }

    /**
     * Установливает массив объектов свойств
     * 
     * @param  array  $propertyList  Макссив объектов свойств
     * 
     * @return  $this
     */
    protected function setProperty(array $propertyList): self
    {
        $this->propertyList = new PropertyCollection($propertyList, $this);

        return $this;
    }

    /**
     * Возвращает коллекцию свойств инфоблока
     * 
     * @return PropertyCollection
     */
    public function getProperty(): PropertyCollection
    {
        return $this->propertyList;
    }

    /**
     * Возвращает коллекцию элементов инфоблока
     * 
     * @param ElementParams|null $params
     * 
     * @return ElementCollection|null
     */
    public static function getElements(?ElementParams $params): ?ElementCollection
    {
        $elements = new ElementCollection(static::getInstance(), $params);

        return $elements;
    }

    /**
     * Возвращает коллекцию разделов инфоблога
     * 
     * @param SectionParams|null $params
     * 
     * @return SectionCollection|null
     */
    public static function getSection(?SectionParams $params = null): ?SectionCollection
    {
        $sections = new SectionCollection(static::getInstance(), $params);

        return $sections;
    }
}
