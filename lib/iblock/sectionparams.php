<?php

declare(strict_types=1);

namespace Initstudio\Core\IBlock;

use Initstudio\Core\Collection\Params;

class SectionParams extends Params
{
    /**
     * @param string|IBlock $iblock
     */
    public function __construct($iblock)
    {
        $this->iblock = $iblock::getInstance();
    }

    /**
     * Возвращает поля для выборки
     * 
     * @return array 
     */
    public function getSelect(): array
    {
        return count($this->select) ? \array_merge(['ID', 'CODE'], $this->select) : ['*'];
    }

    /**
     * Возвращает фильтр для выборки
     * 
     * @return array 
     */
    public function getFilter(): array
    {
        $res = \array_merge(
            $this->filter,
            ['IBLOCK_ID' => $this->iblock::getId()]
        );

        return $res;
    }

    /**
     * Возвращает лимит и постраничную навигацию для выборки
     * 
     * @return false|array 
     */
    public function getLimit()
    {
        $res = false;
        if (isset($this->limit) && empty($this->offset)) {
            $res = ['nTopCount' => $this->limit];
        } elseif (isset($this->limit) && isset($this->offset)) {
            $res = [
                'iNumPage' => $this->offset / $this->limit + 1,
                'nPageSize' => $this->limit
            ];
        }
        return $res;
    }

    /**
     * @param int|array $id
     *
     * @return self
     */
    public function setIblockId($id): self
    {
        $this->iblockId = $id;

        return $this;
    }

    /**
     * @return int|array
     */
    public function getIblockId()
    {
        return $this->iblockId;
    }
}
