<?php

declare(strict_types=1);

namespace Initstudio\Core\IBlock;

class Element extends Base
{
    use ElementTrait;

    /**
     * Ссылка на экземпляр раздела
     * 
     * @var Section|Section[]
     */
    private $section = [];

    /**
     * Массив значений свойств
     * 
     * @var array<string,mixed> массив [символьный код свайства => значение свойства]
     */
    private $propertyValue = [];

    public function __construct(array $data, IBlock &$iblock)
    {
        $this->iblock = $iblock;
        $this->fields = $data;
        $this->fields['IBLOCK_ID'] = $iblock->getId();

        isset($data['CODE']) ? $this->setCode($data['CODE']) : '';
        isset($data['ID']) ? $this->setId((int)$data['ID']) : '';

        if (isset($data['PROPERTY'])) {
            $this->setProperty($data['PROPERTY']);
        }
        $this->isUpdated = false;
    }

    /**
     * Устанавливает поле элемента
     *
     * @param string $field
     * @param mixed $value
     *
     * @return self
     */
    public function setField(string $field, $value): self
    {
        $this->fields[$field] = $value;

        return $this;
    }

    /**
     * Устанавливает свойства элемента
     * 
     * @param array $propertyList 
     * 
     * @return self
     */
    private function setProperty(array $propertyList): self
    {
        foreach ($propertyList as $property) {
            $this->propertyValue[$property['CODE']] = $property;
        }

        return $this;
    }

    /**
     * Создаёт новый раздел и возвращает его объект
     * 
     * @return self
     */
    public function save(): self
    {
        $el = new \CIBlockElement;

        if ($this->getId()) {
            $res = $el->Update($this->getId(), $this->getFields());
            $this->isUpdated = false;
        } else {
            $ID = $el->Add($this->getFields());
            if ($ID > 0) {
                $this->setId($ID);
                $this->isUpdated = false;
            }
        }

        return $this;
    }

    /**
     * Копирует раздел в указанный инфоблок с изменёнными параметрами
     * 
     * @param \Initstudio\Core\IBlock\IBlock $iblock 
     * @param array $fields 
     * 
     * @return null|\Initstudio\Core\IBlock\Element 
     */
    private function copy(IBlock &$iblock, array $fields = []): ?Element
    {
        $newFields = \array_merge_recursive($this->getFields(), $fields);
        unset($newFields['ID']);

        return (new Element($newFields, $iblock))->save();
    }

    /**
     * Устанавливает ссылку на объект раздела
     *
     * @param Section $section
     *
     * @return self
     */
    public function addSection(Section &$section): self
    {
        $this->section[] = $section;

        return $this;
    }
}
