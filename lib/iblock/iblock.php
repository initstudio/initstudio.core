<?php

declare(strict_types=1);

namespace Initstudio\Core\IBlock;

use Bitrix\Main\Data\Cache;
use Bitrix\Main\Loader;
use Initstudio\Core\IBase;
use Initstudio\Core\IBlock\Base;

/**
 * Абстрактный класс для работы с инфоблоками
 * 
 * @method static string getCode()
 * @method static int getId()
 *
 * @author Bondar Maksim <max@initstudio.ru>
 *
 * @copyright 2021 InitStudio
 * 
 * @package Initstudio\Core\IBlock
 */
abstract class IBlock extends Base implements IBase
{
    use IBlockTrait;

    private static $cacheIblockIdPostfix = '_IBLOCK_ID';
    private static $cacheTime = 30 * 60 * 60 * 24;
    private static $cacheDir = '/initstudio/iblock';

    /**
     * @var IBlockCollection
     */
    private static $instances;

    abstract public static function getCodeRaw(): string;

    protected function setCode(string $code): self
    {
        return $this;
    }

    public function getCode(): string
    {
        return static::getCodeRaw();
    }

    protected function getId()
    {
        return $this->id;
    }

    protected function __construct()
    {
        $cache = Cache::createInstance();
        $propertyList = null;
        $id = null;

        if ($cache->initCache(
            self::$cacheTime,
            static::getCode() . self::$cacheIblockIdPostfix,
            self::$cacheDir
        )) {
            [
                'ID' => $id,
                'PROPERTY' => $propertyList,
                'TYPE' => $type
            ] = $cache->getVars();
        } elseif ($cache->startDataCache()) {

            ['ID' => $id, 'IBLOCK_TYPE_ID' => $type] = $this->getIBlockRaw();
            $propertyList = $this->getPropertyRaw();

            $cache->endDataCache([
                'ID' => $id,
                'PROPERTY' => $propertyList,
                'TYPE' => $type
            ]);
        }

        $this->setId((int)$id)
            ->setProperty($propertyList)
            ->setType($type);
    }

    public static function getInstance(): ?Iblock
    {
        $code = static::getCode();
        if (empty(self::$instances)) {
            static::loadModule();
            self::$instances = new IBlockCollection();
        }

        $iblock = self::$instances->byCode($code) ?? self::$instances->addItem(new static)->byCode($code);

        return $iblock;
    }

    public static function __callStatic($name, $arguments)
    {
        switch ($name) {
                //     case 'getIBlock':
                //         return static::getIBlock(...$arguments);
                //         break;

            default:
                if (method_exists(static::class, $name)) {
                    return static::getInstance()->$name(...$arguments);
                }
                break;
        }
    }

    /**
     * Подключает модуль инфоблоков
     * @return bool
     */
    public static function loadModule(): bool
    {
        try {
            Loader::includeModule('iblock');
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Возварщает тип инфоблока
     * @return string 
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * Set тип инфоблока
     * @param  string  Тип инфоблока
     * @return  self
     */
    protected function setType(string $type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Возвращает данные об инфоблоке
     * 
     * @return array 
     * 
     * @throws \Bitrix\Main\ArgumentTypeException 
     * @throws \Bitrix\Main\ArgumentNullException 
     * @throws \Bitrix\Main\Config\ConfigurationException 
     */
    protected function getIBlockRaw(): array
    {
        return \CIBlock::GetList(
            [],
            ['CODE' => static::getCode()]
        )->Fetch();
    }

    public static function getEventList(): array
    {
        return [
            'OnAfterIBlockAdd' => 'iblock',
            'OnAfterIBlockUpdate' => 'iblock',
            'OnBeforeIBlockDelete' => 'iblock',
        ];
    }

    protected function checkIBlock(int $id): bool
    {
        $res = $this->getId() === $id;
        return $res;
    }

    public static function onAfterIBlockAddEventHandler(array $arFields): void
    {
        if (isset($arFields['CODE'])) {
            self::clearCache($arFields['CODE']);
        }
    }

    public static function onAfterIBlockUpdateEventHandler(array $arFields): void
    {
        if (isset($arFields['CODE'])) {
            self::clearCache($arFields['CODE']);
        }
    }

    public static function onBeforeIBlockDeleteEventHandler(int $id): void
    {
        ['CODE' => $code] = \CIBlock::GetByID($id)->Fetch();
        self::clearCache($code);
    }

    protected static function clearCache(string $code): void
    {
        $cache = \Bitrix\Main\Data\Cache::createInstance();
        $cache->clean($code . self::$cacheIblockIdPostfix, self::$cacheDir);
    }
}
