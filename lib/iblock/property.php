<?php

declare(strict_types=1);

namespace Initstudio\Core\IBlock;

/**
 * Класс описывающий свойства элементов инфоблока
 *
 * @author Bondar Maksim <max@initstudio.ru>
 *
 * @copyright 2021 InitStudio
 *
 * @package Initstudio\Core\IBlock
 */
class Property extends Base
{
    use PropertyTrait;

    /**
     * Тип свойства
     * @var string <UL>
     * <LI>S - строка</LI>
     * <LI>N - число</LI>
     * <LI>L - список</LI>
     * <LI>F - файл</LI>
     * <LI>G - привязка к разделу</LI>
     * <LI>E - привязка к элементу</LI>
     * </UL>
     */
    protected $type;

    public function __construct(array $data, &$iblock)
    {
        $this->iblock = $iblock;
        $this->setFields($data);
    }

    /**
     * Устанавливает параметры свойства
     * @param array $data 
     * @return \Initstudio\Core\IBlock\Property 
     */
    private function setFields(array $data): self
    {
        $this->fields = $data;
        $this->fields['IBLOCK_ID'] = $this->iblock->getId();
        isset($data['CODE']) ? $this->setCode($data['CODE']) : '';
        isset($data['ID']) ? $this->setId((int)$data['ID']) : '';
        $this->setType($data['PROPERTY_TYPE']);

        return $this;
    }

    /**
     * Возвращает тип свойства
     * @return  string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Устанавливает тип свойства
     * @param  string  $type
     * @return  self
     */
    public function setType(string $type)
    {
        $this->type = $type;

        return $this;
    }
}
