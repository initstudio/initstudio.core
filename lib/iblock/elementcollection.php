<?php

declare(strict_types=1);

namespace Initstudio\Core\IBlock;

/**
 * Список элементов инфоблока
 * 
 * @property ElementParams|null $params
 * 
 * @method \Initstudio\Core\IBlock\Element byId(int $id)
 *
 * @author Bondar Maksim <max@initstudio.ru>
 *
 * @copyright 2021 InitStudio
 *
 * @package Initstudio\Core\IBlock
 */
class ElementCollection extends CollectionBase
{
    use ElementTrait;

    public function __construct(IBlock &$iblock, ?ElementParams $params)
    {
        $this->iblock = $iblock;
        if ($params) {
            $params->setIblockId($iblock->getId());
            $this->params = $params;
        }
    }

    /**
     * Возвращает коллекцию элементов по их id
     * 
     * @param int|int[] $id
     * 
     * @return ElementCollection
     */
    public function filterId($id): ElementCollection
    {
        $this->params->setFilter(['=ID' => $id]);

        return $this;
    }

    /**
     * Возвращает коллекцию элементов по их символьным кодам
     * 
     * @param string|array<int|string> $code
     * 
     * @return $this 
     */
    public function filterCode($code): ElementCollection
    {
        $this->params['filter']['=CODE'] = $code;

        return $this;
    }

    /**
     * Устанавливает объект выборки
     * 
     * @return \CIBlockResult
     * 
     * @throws \Bitrix\Main\ArgumentOutOfRangeException 
     * @throws \Bitrix\Main\LoaderException 
     * @throws \Bitrix\Main\Config\ConfigurationException 
     * @throws \Bitrix\Main\ObjectNotFoundException 
     * @throws \Bitrix\Main\ArgumentNullException 
     */
    private function getGetListResult(): \CIBlockResult
    {
        $params = $this->params;
        $res = \CIBlockElement::GetList(
            $params->getOrder(),
            $params->getFilter(),
            $params->getGroup(),
            $params->getLimit(),
            $params->getSelect()
        );

        return $res;
    }

    /**
     * Возвращает результаты выборки списка
     * 
     * @return array
     */
    public function getList(): array
    {
        $res = self::fetchAll($this->getGetListResult());
        $link = [];
        foreach ($res as $key => $element) {
            $this->addItem(new Element($element, $this->iblock));
            $link[$element['ID']] = &$res[$key];
        }
        if (count($this->params->getSelectProperty()) && count($res)) {
            $propertyRes = $this->getPropertyRaw();
            foreach ($propertyRes as $elementId => $propertyValue) {
                $link[$elementId]['PROPERTY'] = $propertyValue;
            }
        }

        return $res;
    }

    /**
     * Возвращает результаты выборки строки
     * 
     * @return array|bool
     */
    public function getRow()
    {
        $oldLimit = $this->params->getLimit();
        $this->params->setLimit(1);
        $res = $this->getGetListResult()->Fetch();
        $this->params->setLimit($oldLimit);

        if ($res) {
            if (count($this->params->getSelectProperty()) && count($res)) {
                $propertyRes = $this->getPropertyRaw();
                $res['PROPERTY'] = $propertyRes[$res['ID']];
            }
            $this->addItem(new Element($res, $this->iblock));
        }

        return $res;
    }

    /**
     * Возвращает результаты выборки свойств элементов
     * 
     * @return array
     */
    private function getPropertyRaw(): array
    {
        $result = [];
        $filter = count($this->params->getSelectProperty()) ? ['CODE' => $this->params->getSelectProperty()] : [];
        $elementsFilter = ['ID' => $this->getId()];
        if (!empty($elementsFilter)) {
            \CIBlockElement::GetPropertyValuesArray(
                $result,
                $this->iblock->getId(),
                $elementsFilter,
                $filter
            );
        }

        return $result;
    }

    /**
     * Возвращает значения всех свойств или по переданномупараметру
     * 
     * @param int|string|array $property свойства для выборки, чувствителен к типу передаваемого параметра
     * 
     * @return array 
     */
    public function getProperty($property = null): array
    {
        if (isset($property)) {
            switch (gettype($property)) {
                case 'integer':
                    $this->params['property'] = $property;
                    break;

                case 'array':
                    foreach ($property as $value) {
                        if ($id = $this->iblock->getProperty()->byCode($value) ?:
                            $this->iblock->getProperty()->byId(\intval($value))
                        ) {
                            $this->params['property'] = $id->getId();
                        }
                    }
                    break;

                case 'string':
                default:
                    $this->params['property'] = $this->iblock
                        ->getProperty()
                        ->byCode($property)
                        ->getId();
                    break;
            }
        } else {
            $this->params['property'] = $this->params['property'] ?? [];
        }
        $res = $this->getList();

        return $res;
    }

    /**
     * Возвращает массив, описывающий поля разделов
     *
     * @return array
     */
    public function getSections(): array
    {
        $sectionCollection = $this->iblock::getSection();
        $elementSectionList = $sectionCollection->getElementSection($this);

        foreach ($elementSectionList as $elementId => $sectionList) {
            foreach ($sectionList as $section) {
                $this->byId($elementId)
                    ->addSection($sectionCollection->byId($section['ID']));
            }
        }

        return $elementSectionList;
    }
}
