<?php

declare(strict_types=1);

namespace Initstudio\Core\IBlock;

/**
 * Абстранктный класс объекта
 *
 * @author Bondar Maksim <max@initstudio.ru>
 *
 * @copyright 2021 InitStudio
 *
 * @package Initstudio\Core\IBlock
 */
abstract class Base
{
    /**
     * Идентификатор свойства
     * 
     * @var int|null
     */
    protected $id = null;

    /**
     * Символьный код свойства
     * 
     * @var string|string[]|null
     */
    protected $code = null;

    /**
     * Параметры
     * @var array
     */
    protected $fields = [];

    /**
     * Ссылка на объект инфоблока
     * 
     * @var \Initstudio\Core\IBlock\IBlock|null
     */
    protected $iblock = null;

    /**
     * Устанавливает идентификатор
     * 
     * @param int $id
     * 
     * @return self
     */
    protected function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Возвращает список идентификаторов
     * 
     * @return int
     */
    protected function getId()
    {
        return $this->id;
    }

    /**
     * Устанавливает символьный код свойства
     * 
     * @param string $code
     * 
     * @return self
     */
    protected function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Возвращает список символьных кодов
     * 
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Возвращает поля объекта
     * 
     * @return array 
     */
    protected function getFields(): array
    {
        return $this->fields;
    }

    /**
     * Возвращает результаты выборки
     * 
     * @param \CDBResult $res
     * 
     * @return array 
     */
    protected static function fetchAll(\CDBResult $res): array
    {
        $ar = [];
        while ($tmp = $res->Fetch()) {
            $ar[] = $tmp;
        }

        return $ar;
    }
}
