<?php

declare(strict_types=1);

namespace Initstudio\Core\IBlock;

/**
 * Код для работы с элементами
 * 
 * @method int|int[] getId()
 *
 * @author Bondar Maksim <max@initstudio.ru>
 *
 * @copyright 2021 InitStudio
 *
 * @package Initstudio\Core\IBlock
 */
trait ElementTrait
{
}
