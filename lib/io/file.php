<?php

declare(strict_types=1);

namespace Initstudio\Core\IO;

/**
 * Работает с таблицей фалов
 *
 * @author Bondar Maksim <max@initstudio.ru>
 *
 * @copyright 2021 InitStudio
 *
 * @package Initstudio\Core\IO
 */
class File
{
    /**
     * Объект выборки
     * @var \CDBResult
     */
    protected $res;

    /**
     * Параметры для выборки
     * @var array
     */
    protected $params = [];

    /**
     * Принимает паратемтры для выборки
     *
     * @param array $params
     */
    public function __construct(array $params = [])
    {
        $this->setParams($params);
    }

    protected function setParams(array $params)
    {
        $this->setOrder($params['order'] ?? []);
        $this->setFilter($params['filter'] ?? []);
        return $this;
    }

    protected function setOrder(array $order): self
    {
        $this->params['order'] = $order;
        return $this;
    }

    protected function getOrder(): array
    {
        return $this->params['order'];
    }

    protected function setFilter(array $filter): self
    {

        foreach ($filter as $key => $value) {
            if (\is_array($value)) {
                $filter['@' . $key] = \implode(',', $value);
                unset($filter[$key]);
            }
        }
        $this->params['filter'] = $filter;
        return $this;
    }

    protected function getFilter(): array
    {
        return $this->params['filter'];
    }

    protected function setResult(): self
    {
        $this->res = \CFile::GetList(
            $this->getOrder(),
            $this->getFilter()
        );
        return $this;
    }

    public function getRow(array $params): array
    {
        return $this->getList($params)[0];
    }

    public static function getList(array $params): array
    {
        $file = new self($params);
        return $file->setResult()->fetchAll();
    }

    protected function fetch(): ?array
    {
        return $this->res->Fetch() ?: null;
    }

    protected function fetchAll(): array
    {
        $res = [];
        while ($tmp = $this->fetch()) {
            $tmp['SRC'] = self::getPath($tmp);
            $res[] = $tmp;
        }
        return $res;
    }

    /**
     * Возврщает путь к файлу
     * @param mixed $file идентификатор файла или описывающий его массив
     * @return string путь к файлу
     */
    public static function getPath($file): string
    {
        $path = '';

        if (\is_integer($file) || \is_string($file)) {
            [$file => $path] = self::getListPath([$file]);
        }

        if (\is_array($file)) {
            [
                'SUBDIR' => $subdir,
                'FILE_NAME' => $fileName
            ] = $file;
        }

        if ($subdir && $fileName) {
            $path = '/upload' . "/" . $subdir . "/" . $fileName;
        }

        return $path;
    }

    /**
     * Возвращает список питей к файлам
     * @param array $listId массив идентификаторов файлов
     * @return array<integer,string> ключ - идентификатор файла, значение - путь к файлу
     */
    public static function getListPath(array $listId): array
    {
        $listPath = [];
        $fileRs = \CFile::GetList(
            [],
            ['@ID' => implode(
                ',',
                $listId
            )]
        );
        while ($tmp = $fileRs->Fetch()) {
            $listPath[$tmp['ID']] = self::getPath($tmp);
        }

        return $listPath;
    }
}
