<?php

declare(strict_types=1);

namespace Initstudio\Core\Collection;

use Initstudio\Core\Text\StringHelper;

/**
 * Объект параметров для выборки
 * 
 * @method self setSelect(array select)
 * @method self setFilter(array filter)
 * @method self setOrder(array order)
 * @method self setGroup(array group)
 * @method self setOffset(int offset)
 * @method self setLimit(int limit)
 * @method array getSelect()
 * @method array getFilter()
 * @method array getOrder()
 * @method array getGroup()
 * @method int getOffset()
 * @method int getLimit()
 *
 * @author Bondar Maksim <max@initstudio.ru>
 *
 * @copyright 2021 InitStudio
 *
 * @package Initstudio\Core\Collection
 */
class Params
{
    protected ?array $select = ['*'];
    protected ?array $filter = [];
    protected array $order = [];
    protected $group = false;
    protected ?int $limit = null;
    protected ?int $offset = null;

    public function __call($name, $arguments)
    {
        if (\strpos($name, 'set') === 0) {
            $var = StringHelper::snakeToCamel(\substr_replace($name, '', 0, 3));
            if (property_exists($this, $var)) {
                $this->$var = $arguments[0];
            }
        } elseif (\strpos($name, 'get') === 0) {
            $var = StringHelper::snakeToCamel(\substr_replace($name, '', 0, 3));
            if (property_exists($this, $var)) {
                return $this->$var;
            }
        }

        return $this;
    }
}
