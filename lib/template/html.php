<?php

declare(strict_types=1);

namespace Initstudio\Core\Template;

class Html
{
    public function tag()
    {
    }

    public static function attr($paramList): string
    {
        return is_array($paramList) ?  implode(' ', $paramList) : (string)$paramList;
    }

    public static function attrList(
        array $attrList = [],
        array $params = [
            'separator' => ' ',
            'assignment' => '=',
            'quote' => '"'
        ]
    ): string {
        $res = [];
        [
            'separator' => $separator,
            'assignment' => $assignment,
            'quote' => $quote
        ] = $params;
        foreach ($attrList as $attr =>  $value) {
            $val = '';
            switch ($attr) {
                case 'style':
                    $val = self::attrList($value, [
                        'separator' => ';',
                        'assignment' => ':',
                        'quote' => ''
                    ]);
                    break;

                default:
                    $val = self::attr($value);
                    break;
            }
            $res[] = $attr . $assignment . $quote . $val . $quote;
        }
        return implode($separator, $res);
    }
}
