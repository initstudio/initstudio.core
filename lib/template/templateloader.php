<?php

declare(strict_types=1);

namespace Initstudio\Core\Template;

use Initstudio\Core\Ajax\BackgroundLoad;
use Initstudio\Core\IBase;

/**
 * Инструменты для работы с шаблонами сайтов
 *
 * @author Bondar Maksim <max@initstudio.ru>
 *
 * @copyright 2021 InitStudio
 *
 * @package Initstudio\Core\Template
 */
class TemplateLoader implements IBase
{
    protected static $eventList = [
        'OnPageStart' => 'main'
    ];

    public static function getEventList(): array
    {
        return self::$eventList;
    }

    /**
     * Обработчик события OnPageStart
     *
     * @return bool
     */
    public static function onPageStartEventHandler(): bool
    {
        return static::checkLoadType();
    }

    /**
     * Проверяет тип http запорса и устанавливает нужный шаблон сайта
     *
     * @return bool
     */
    protected static function checkLoadType(): bool
    {
        if (BackgroundLoad::isBgLoad()) {
            define('SITE_TEMPLATE_ID', 'empty');
        }

        return true;
    }
}
