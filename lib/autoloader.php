<?php

declare(strict_types=1);

namespace Initstudio\Core;

use Initstudio\Core\Events\EventManager;

/**
 * Регистрирует автозагрузку своих классов
 *
 * @author Bondar Maksim <max@initstudio.ru>
 *
 * @copyright 2021 InitStudio
 *
 * @package InitStudio\Core
 */
class Autoloader
{
    /**
     * Карта пространств имён
     *
     * @var string|null
     */

    protected ?string $namespace = null;

    /**
     * Список классов с эвентами
     *
     * @var array<int,stdClass>
     */
    protected array $events = [];

    /**
     * Путь к папке с кодом
     *
     * @var string[]
     */
    protected array $rootDir = [];

    /**
     * @var array<string,Autoloader>
     */
    protected static array $instances = [];

    /**
     * Добавляет автозагрузку кастомных классов
     *
     * @param string $namespace пространство имён
     *
     * @return self
     */
    public static function addNamespace(string $namespace): self
    {
        if (empty(self::$instances[$namespace])) {
            self::$instances[$namespace] = new self();
            self::$instances[$namespace]
                ->setNamespace($namespace);
        }

        return self::$instances[$namespace];
    }

    /**
     * Регистрирует автозагрузчик
     *
     * @return self
     */
    public function register(): self
    {
        if (empty($this->getRootDir())) {
            $this->setRootDir($_SERVER['DOCUMENT_ROOT'] .  '/local/php_interface/src');
        }
        spl_autoload_register(array($this, 'autoload'), true, true);

        return $this;
    }

    protected function autoload($class): bool
    {
        $pathParts = explode('\\',  $class);

        if (is_array($pathParts)) {

            $namespaceArray = explode('\\',  $this->getNamespace());
            $namespace = \implode('\\', array_slice($pathParts, 0, count($namespaceArray)));

            if (!empty(self::$instances[$namespace])) {
                foreach ($this->getRootDir() as $rootDir) {
                    $filePath = $rootDir .
                        '/' .
                        implode(
                            '/',
                            array_slice(
                                $pathParts,
                                count($namespaceArray)
                            )
                        ) .
                        '.php';

                    $fileExists = \file_exists($filePath) ?? \file_exists(strtolower($filePath));

                    if ($fileExists) {
                        require_once $filePath;
                        return true;
                    }
                }
            }
        }

        return false;
    }

    private function getNamespace(): string
    {
        return $this->namespace;
    }

    private function setNamespace(string $namespace)
    {
        $this->namespace = $namespace;

        return $this;
    }

    private function getRootDir(): array
    {
        return $this->rootDir;
    }

    /**
     * Устанавливает путь до папки с классами
     *
     * @param string $rootDir Путь к папке с кодом
     *
     * @return self
     */
    public function setRootDir(string $rootDir): self
    {
        try {
            if (empty($this->getNamespace())) {
                throw new \Exception('Не установлено пространство имён', 1);
            }
            if (is_dir($rootDir)) {
                $this->rootDir[] = $rootDir;
            } else {
                throw new \Exception('Не верный путь', 1);
            }
        } catch (\Throwable $th) {
            // throw $th->;
        }

        return $this;
    }

    private function getEvents(): array
    {
        return $this->events;
    }

    /**
     * Задаёт список классов с обработчиками событий
     *
     * @param array $events Список классов с эвентами
     *
     * @return self
     */
    public function setEvents(array $events): self
    {
        $this->events = $events;
        EventManager::initAutoloadEvents($events);

        return $this;
    }
}
